import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import no.geosoft.cc.graphics.GObject;
import no.geosoft.cc.graphics.GPosition;
import no.geosoft.cc.graphics.GScene;
import no.geosoft.cc.graphics.GSegment;
import no.geosoft.cc.graphics.GStyle;
import no.geosoft.cc.graphics.GText;
import no.geosoft.cc.graphics.GWindow;

/**
 *
 * 
 * @author Bashima Islam
 */
public class Hanoi extends JFrame {
	/**
	 * 
	 */
	private TowersOfHanoi towersOfHanoi_;
	private GWindow window_;
	private Peg[] pegs_;
	private int nDiscs_;
	private int nPegs_;

	public Hanoi(int nDiscs, int Pegs) {
		super("Tower of Hanoi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		nDiscs_ = nDiscs;
		nPegs_ = Pegs;
		// Create the graphic canvas
		window_ = new GWindow(new Color(255, 255, 255));
		getContentPane().add(window_.getCanvas());

		// Create scene
		GScene scene = new GScene(window_);
		double w0[] = { 0.0, 0.0, 0.0 };
		double w1[] = { nPegs_ + 1, 0.0, 0.0 };
		double w2[] = { 0.0, nDiscs_ * 2, 0.0 };
		scene.setWorldExtent(w0, w1, w2);

		// Add title object and add to scene
		scene.add(new Title());

		// Create the pegs and add to the scene
		int nPegs = Pegs;
		pegs_ = new Peg[nPegs];
		for (int i = 0; i < nPegs; i++) {
			pegs_[i] = new Peg(i + 1.0);
			scene.add(pegs_[i]);
		}

		// Create the discs and add to the first peg
		for (int i = 0; i < nDiscs; i++) {
			Disc disc = new Disc((double) (nDiscs - i) / nDiscs);
			disc.setPosition(1.0, i);
			pegs_[0].add(disc);
		}

		pack();
		setSize(new Dimension(500, 500));
		setVisible(true);

		// Create the puzzle and execute the solution
		towersOfHanoi_ = new TowersOfHanoi();
		towersOfHanoi_.solve();
	}

	public void discMoved(int source, int destination) {
		// This is the disc to move
		Disc disc = (Disc) pegs_[source]
				.getChild(pegs_[source].getNChildren() - 1);

		double y0 = disc.getY();
		double y1 = nDiscs_ + 4.0;

		double x0 = pegs_[source].getX();
		double x1 = pegs_[destination].getX();

		// Animate vertical up movement
		double step = 0.1;
		double y = y0;
		while (y < y1) {
			disc.setPosition(x0, y);
			disc.redraw();
			window_.refresh();
			y += step;
		}

		// Reparent peg
		pegs_[source].remove(disc);
		pegs_[destination].add(disc);

		// Animate horizontal movement
		step = 0.01;
		double x = x0;
		while (x != x1) {
			disc.setPosition(x, y);
			disc.redraw();
			window_.refresh();
			x += (x1 > x0 ? step : -step);
			if (Math.abs(x - x1) < 0.01)
				x = x1;
		}

		// Animate vertical down movement
		step = 0.2;
		y = y1;
		y1 = pegs_[destination].getNChildren() - 1;
		while (y > y1) {
			if (Math.abs(y - y1) < 0.01)
				y = y1;
			disc.setPosition(x, y);
			disc.redraw();
			window_.refresh();
			y -= step;
		}
	}

	/**
	 * Graphics object for canvas title.
	 */
	class Title extends GObject {
		private GSegment anchor_;

		public Title() {
			GStyle style = new GStyle();
			style.setLineStyle(GStyle.LINESTYLE_INVISIBLE);
			style.setForegroundColor(new Color(100, 100, 200));
			style.setFont(new Font("serif", Font.PLAIN, 36));
			setStyle(style);

			anchor_ = new GSegment();
			addSegment(anchor_);

			GText text = new GText("Towers of Hanoi", GPosition.SOUTHEAST);
			anchor_.setText(text);
		}

		public void draw() {
			anchor_.setGeometry(20, 20);
		}
	}

	/**
	 * Graphics representation of a peg.
	 */
	class Peg extends GObject {
		private double x_;
		private GSegment peg_;
		private double[] xy_;

		public Peg(double x) {
			x_ = x;

			GStyle style = new GStyle();
			style.setBackgroundColor(new Color(150, 150, 150));
			setStyle(style);

			peg_ = new GSegment();
			addSegment(peg_);

			xy_ = new double[] { x_ - 0.05, 0.0, x_ - 0.05, nDiscs_ + 2,
					x_ + 0.05, nDiscs_ + 2, x_ + 0.05, 0.0, x_ - 0.05, 0.0 };
		}

		public double getX() {
			return x_;
		}

		public void draw() {
			peg_.setGeometryXy(xy_);
		}
	}

	/**
	 * Graphics representation of a disc.
	 */
	class Disc extends GObject {
		private double size_;
		private GSegment disc_;
		private double x_, y_;

		public Disc(double size) {
			size_ = size;

			GStyle style = new GStyle();
			style.setForegroundColor(new Color(60, 210, 225));
			style.setBackgroundColor(new Color(80, 150, 221));
			setStyle(style);

			disc_ = new GSegment();
			addSegment(disc_);
		}

		public void setPosition(double x, double y) {
			x_ = x;
			y_ = y;
		}

		public double getY() {
			return y_;
		}

		public void draw() {
			double[] xy = new double[] { x_ - size_ / 2.0, y_,
					x_ - size_ / 2.0, y_ + 1.0, x_ + size_ / 2.0, y_ + 1.0,
					x_ + size_ / 2.0, y_, x_ - size_ / 2.0, y_ };

			disc_.setGeometryXy(xy);
		}
	}

	/**
	 * Class for solving the "Towers of Hanoi" puzzle.
	 */
	class TowersOfHanoi {
		public void solve() {
			ArrayList<Integer> free_peg = new ArrayList<Integer>();
			for (int i = 1; i < nPegs_ - 1; i++) {
				free_peg.add(i);
			}
//			solve(nDiscs_, nPegs_, 1, 3, 2);
			 solve(nDiscs_, 0, nPegs_ - 1, free_peg);
		}
//
//		private void solve(int nDiscs, int nPegs, int source, int destination,
//				int auxiliary) {
//			if (nDiscs == 1)
//				discMoved(source, destination);
//
//			else if (nDiscs > 1) {
//				solve(nDiscs - 1, nPegs, source, auxiliary, destination);
//				discMoved(source, destination);
//				solve(0, nPegs, source, destination, auxiliary);
//				
//				solve(nDiscs - 1, nPegs, auxiliary, destination, source);
//			}
//		}

		public void solve(int number_of_disks, int source, int dest,
				ArrayList<Integer> free_peg) {
			ArrayList<Integer> local_free_peg = new ArrayList<Integer>(free_peg);
			int p, middle;

			if (number_of_disks == 1) {
				discMoved(source, dest);
				// local_free_peg.add(0,source);
				System.out.println("one disk move: " + local_free_peg.size()
						+ " ");
			}

			else {
				if (local_free_peg.size() >= 2)
					p = number_of_disks / 2;
				else
					p = number_of_disks - 1;

				// Move top "p" disks from peg 1 to peg i
				middle = local_free_peg.get(local_free_peg.size() - 1);
				System.out.println("middle: " + middle + " "
						+ local_free_peg.size());

				local_free_peg.remove((local_free_peg.size() - 1));
				local_free_peg.add(dest);
				System.out.println("while moving p to middle: "
						+ local_free_peg.size() + " " + middle);
				solve(p, source, middle, local_free_peg);

				// Move "n - p " disks from peg 1 to another peg

				local_free_peg.remove((local_free_peg.size() - 1));
				System.out.println("while moving n-p: " + local_free_peg.size()
						+ " " + middle);
				solve(number_of_disks - p, source, dest, local_free_peg);

				// Move p from current peg to the final peg
				local_free_peg.add(source);
				System.out.println("while moving p to dest: "
						+ local_free_peg.size() + " " + middle);
				solve(p, middle, dest, local_free_peg);
			}
		}
	}

	public static void main(String[] args) {
		String i = JOptionPane.showInputDialog(
				"Please enter the number of disks", 8);
		int nDiscs = Integer.parseInt(i);
		i = JOptionPane.showInputDialog("Please enter the number of pegs", 4);
		int nPegs = Integer.parseInt(i);
		new Hanoi(nDiscs, nPegs);
	}
}
